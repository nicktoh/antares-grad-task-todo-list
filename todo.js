// Convert to a number
var counter = 1*1;
var taskStatus = "Not started";

var now = new Date();
var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

var startWeekDate = new Date(now);
startWeekDate.setDate(now.getDate() - now.getDay());

var endWeekDate = new Date(now);
endWeekDate.setDate(now.getDate() + (6-now.getDay()));

$(document).ready(function(){
	getCounter();
	loadTasks();
	$("#due-date").datepicker("update", new Date());
	$("table").css("width", $(window).width()*(11/12));
	$(window).resize(function(){
		$("table").css("width", $(window).width()*(11/12));
	});

	$("#add-task").click(function(){
		addTask();			
	});

	$("#task-title").keypress(function(e){
		// Enter button
		if(e.which == 13){
			addTask();
		}
	});
	
	// Delete all tasks button, which is in the settings menu
	$("#delete-all").click(function(){
		localStorage.clear();
		counter = 1;
		location.reload();
	});

	// Recording the selected option for the status dropdown menu
	$(".dropdown-menu li a").click(function(){
		taskStatus = $(this).text();
		$("#select-status").text(taskStatus);
		$("#select-status").append(" <span class='caret'></span>");
	});

	$(".task-list").on("click", ".removeTask", function(){
		var key = $(this).closest("tr").attr("id");
		var taskStatus = localStorage[key + "Status"];
		localStorage.removeItem(key + "Title");
		localStorage.removeItem(key + "DueDate");
		localStorage.removeItem(key + "Status");
		$(this).closest("tr").remove();

		if($("#task-list-wrapper > div > table > tbody > tr").length == 0){
			$("#task-list-wrapper").hide();
			$("#no-tasks").show();
		}
		else if($("#tasks-today > table > tbody > tr").length == 0){
			$("#task-list-today").hide();
			$("#today-header").hide();
			$("#no-tasks-today").show();
		}
		else if($("#tasks-week > table > tbody > tr").length == 0){
			$("#tasks-week").hide();
		}

		$("#all-tasks-badge").text(Math.floor(localStorage.length/3));
		taskStatus = taskStatus.trim();
		if(taskStatus == "Not started"){
			$("#not-started-badge").text(($("#not-started-badge").text()*1)-1);
		}
		else if(taskStatus == "In progress"){
			$("#in-progress-badge").text(($("#in-progress-badge").text()*1)-1);
		}
		else if(taskStatus == "Done"){
			$("#done-badge").text(($("#done-badge").text()*1)-1);
		}
	});

	// Editing a task name
	$(".task-list").on("click", ".title", function(){
		var text = $(this).text();
		$(this).replaceWith("<td>" +
			"<input type='text' class='form-control edit-task-submit' value='" + text + "'></input>" + 
			"<button class='btn btn-success edit-task'><span class='glyphicon glyphicon-floppy-disk'></span></button>" + 
			"</td>");
	});

	// Saving task title changes
	$(".task-list").on("click", ".edit-task", function(){
		var key = $(this).closest("tr").attr("id");
		var newText = $(this).parent().parent().find("input").val();
		localStorage[key + "Title"] = newText;
		$(this).parent().replaceWith("<td class='title'>" + newText + "</td>");
	});

	// For the status button groups of each task
	$(".task-list").on("click", ".btn-status", function(){
		var key = $(this).parent().parent().parent().attr("id") + "Status";

		// Change appearance
		$(this).parent().children().removeClass("active");
		$(this).addClass("active");

		// Writing to local storage
		var icon = $(this).find(".glyphicon").attr("class");
		// Remove glyphicon class so we can access the subclass
		icon = icon.substring(9,icon.length).trim();
		if(icon == "glyphicon-inbox"){
			localStorage[key] = "Not started";
			$(this).parent().find(".taskStatus").text("Not started");
		}
		else if(icon == "glyphicon-hourglass"){
			localStorage[key] = "In progress";
			$(this).parent().find(".taskStatus").text("In progress");
		}
		else if(icon == "glyphicon-check"){
			localStorage[key] = "Done";
			$(this).parent().find(".taskStatus").text("Done");
		}
	});

	$("#trigger-add-task").click(function(){
		$(this).fadeOut("fast");
		$("#add-task-form").show("slow");
		$("#task-title").focus();
	});

	$("#cancel-add-task").click(function(){
		$(this).closest("#add-task-form").hide("slow");
		$("#trigger-add-task").fadeIn("slow");
		$("#task-title").val("");
		$(".error-msg").hide();
	});

	$("input[name='due-date']").datepicker({
		autoclose: true,
		format: "dd MM yyyy"
	});

	$("#today-header").text("Today: " + days[now.getDay()] + " " + now.getDate() + " " + months[now.getMonth()]);
	$("#week-header").text("This week: " + startWeekDate.getDate() + "-" + endWeekDate.getDate() + " " + months[now.getMonth()]);

	$("#mobile-menu").click(function(){
		$("#sidebar").toggleClass("hidden-xs");
		$("#sidebar").toggleClass("hidden-sm");
		$("#sidebar").toggleClass("hidden-md");

		if($("#sidebar").hasClass("hidden-xs")){
			$("#sidebar").show("slow");
			$("#sidebar").hide("slow");
		}
		else{
			$("#sidebar").hide();
			$("#sidebar").show("slow");
		}
	});

	$(".status-sidebar-text").click(function(){
		$("#task-list-wrapper > div > table > tbody > tr").remove();
		var status = $(this).text().trim();
		for (var i = 1; i < counter; i++) {
			var key = "Item" + i;

			// Test existence of task key by attempting to get the title
			if(localStorage[key+"Title"] && localStorage[key + "Status"].trim() == status){
				var title = localStorage[key + "Title"];
				var dueDate = localStorage[key + "DueDate"];
				var taskStatus = localStorage[key + "Status"];
				var dateGroup = getDateGroup(dueDate);
				displayTask(key, title, dueDate, taskStatus, dateGroup);
			}		
		}
		// if(status == "Not started"){
			taskStatus = status;
			$("#select-status").text(status);
		// }
	});

	$("#all-tasks-sidebar").click(function(){
		$("#task-list-wrapper > div > table > tbody > tr").remove();
		loadTasks();
	})
});

function addTask() {
	var key = "Item" + counter;

    /*
     * Task title
     */
    var title = $("#task-title").val();
    if(title == ""){
    	$("#task-title-empty").show();
    	return;
    }
    else{
    	// All good, so no need to show it
    	$("#task-title-empty").hide();
    }

    /*
     * Due date
     */

    $("#due-date").datepicker("update");
    var dueDate = new Date($("#due-date").datepicker("getDate"));

    if(dueDate == ""){
    	$("#due-date-empty").show();
    	return;
    }
    // Checking validity
    else if(dueDate.getDate() < now.getDate()){
    	$("#due-date-empty").hide();
    	$("#due-date-invalid").show();
    	return;
    }
    else{
    	// All good
    	$("#due-date-empty").hide();
    	$("#due-date-invalid").hide();
    }

    // Format date
    dueDate = dueDate.toString().substring(0,11);

    /*
     * Status
     */
    if(taskStatus == ""){
    	$("#status-empty").show();
    	return;
    }
    else{
    	$("#status-empty").hide();
    }

    localStorage[key + "Title"] = title;
    localStorage[key + "DueDate"] = dueDate;
    localStorage[key + "Status"] = taskStatus;
    var dateGroup = getDateGroup(dueDate);

    $("#no-tasks").hide();
    $("#task-list-wrapper").show();
    displayTask(key, title, dueDate, taskStatus,dateGroup);

    // Convert to a number before doing numerical operations
    counter = counter*1;
	counter += 1;
	localStorage["counter"] = counter;

	$("#all-tasks-badge").text(Math.floor(localStorage.length/3));
	if(taskStatus == "Not started"){
		$("#not-started-badge").text(($("#not-started-badge").text()*1)+1);
	}
	else if(taskStatus == "In progress"){
		$("#in-progress-badge").text(($("#in-progress-badge").text()*1)+1);
	}
	else if(taskStatus == "Done"){
		$("#done-badge").text(($("#done-badge").text()*1)+1);
	}

	// Prepare for next task
	$("#add-task-form").hide("fast");
	$("#trigger-add-task").fadeIn("slow");
	$("#task-title").val("");
	$("#due-date").datepicker("update", new Date());
	$("#addTaskModal").modal("hide");
}

// Formats string as YYYY/MM/DD
function getFormattedDate(){
	return now.toString().substring(0,11);
}

function getCounter() {
	if (typeof(Storage) !== "undefined") {
		if(localStorage.getItem("counter") === null){
			localStorage["counter"] = 1;
		}
		else{
			counter = localStorage["counter"];
		}
	}
	else{
		alert("Unfortunately your browser does not support Web Storage, so stay on this page to keep your todos.");
	}
}

function displayTask(key, title, dueDate, taskStatus, dateGroup){
	var taskStatusBtns = "";
	if(taskStatus.trim() == "Not started"){

		taskStatusBtns = "<span class='taskStatus'>" + taskStatus +  "</span> " +
			"<button class='btn btn-default btn-status btn-status" + key + " active'><span class='glyphicon glyphicon-inbox'></span></button>" + 
			"<button class='btn btn-default btn-status btn-status" + key + "'><span class='glyphicon glyphicon-hourglass'></span></button>" +
			"<button class='btn btn-default btn-status btn-status" + key + "'><span class='glyphicon glyphicon-check'></span></button>";
	}
	else if(taskStatus.trim() == "In progress"){
		taskStatusBtns = "<span class='taskStatus'>" + taskStatus +  "</span> " +
			"<button class='btn btn-default btn-status btn-status" + key + "'><span class='glyphicon glyphicon-inbox'></span></button>" + 
			"<button class='btn btn-default btn-status btn-status" + key + " active'><span class='glyphicon glyphicon-hourglass'></span></button>" +
			"<button class='btn btn-default btn-status btn-status" + key + "'><span class='glyphicon glyphicon-check'></span></button>";
	}
	else if(taskStatus.trim() == "Done"){
		taskStatusBtns = "<span class='taskStatus'>" + taskStatus +  "</span> " +
			"<button class='btn btn-default btn-status btn-status" + key + "'><span class='glyphicon glyphicon-inbox'></span></button>" + 
			"<button class='btn btn-default btn-status btn-status" + key + "'><span class='glyphicon glyphicon-hourglass'></span></button>" +
			"<button class='btn btn-default btn-status btn-status" + key + " active'><span class='glyphicon glyphicon-check'></span></button>";
	}

	var taskRow = "<tr class='task' id='" + key + "'>" + 
		"<td class='title'>" + title + "</td>" + 
		"<td class='dueDate'>" + dueDate + "</td>" + 
		"<td>" + "<div class='btn-group'>" + taskStatusBtns + "</div></td>" +
		"<td><button class='btn btn-danger removeTask'><span class='glyphicon glyphicon-remove'></span></button></td>" +
		"</tr>";

	// Determine which table to display in
	if(dateGroup == "Today"){
		$("#task-list-today").append(taskRow);
		$("#task-list-today").show();
		$("#tasks-today").show();
	}
	else if(dateGroup == "Week"){
		$("#task-list-week").append(taskRow);
		$("#tasks-week").show();
	}
}

function loadTasks() {
	counter = counter*1
	var notStartedTasks = 0;
	var inProgressTasks = 0;
	var doneTasks = 0;

	if(localStorage.length == 1){
		$("#no-tasks").show();
		return;
	}
	else{
		$("#task-list-wrapper").show();
	}

	for (var i = 1; i < counter; i++) {
		var key = "Item" + i;
		// Test existence of task key by attempting to get the title
		if(localStorage[key+"Title"]){
			var title = localStorage[key + "Title"];
			var dueDate = localStorage[key + "DueDate"];
			var taskStatus = localStorage[key + "Status"];
			var dateGroup = getDateGroup(dueDate);
			displayTask(key, title, dueDate, taskStatus, dateGroup);

			taskStatus = taskStatus.trim();
			if(taskStatus == "Not started"){
				notStartedTasks++;
			}
			else if(taskStatus == "In progress"){
				inProgressTasks++;
			}
			else if(taskStatus == "Done"){
				doneTasks++;
			}
		}		

		$("#not-started-badge").text(notStartedTasks);
		$("#in-progress-badge").text(inProgressTasks);
		$("#done-badge").text(doneTasks);
	}

	$("#all-tasks-badge").text(Math.floor(localStorage.length/3));
}

function getDateGroup(dueDate) {
	var dateToCompare = new Date();

	if(dueDate == getFormattedDate()){
		$("#no-tasks-today").hide();
		$("#today-header").show();
		return "Today";
	}

	dateToCompare = endWeekDate;
	dueDate = new Date(dueDate + " " + now.getFullYear());

	if(dueDate < dateToCompare){
		$("#tasks-week").show();
		return "Week";
	}

	return "Unknown";
}