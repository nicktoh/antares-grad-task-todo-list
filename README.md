# README #

TODO List is an application that helps you keep track of your tasks. It allows you to add, edit and remove tasks.

### Architecture ###

The application is built on:

* HTML
* CSS/Bootstrap
* JavaScript/jQuery
* Browser local storage: to store the tasks
* [Google Web Fonts](https://www.google.com/fonts) 
    * Source Sans Pro for the body text 
    * Indie Flower for the navigation bar text

### Author ###

Nicholas Toh

### Running the application ###

Open up the todo.html file 

* todo.css and todo.js is linked in todo.html

### todo.js functions ###

`addTask()` : Gets the form field values, validates them, stores them in local storage and updates the sidebar badges

* Returns: nothing

`getFormattedDate()` : Returns a string of the current date in the format of "YYYY/MM/DD"

* Returns: date- string

`getCounter()` : Fetches the counter from local storage and stores it into the counter variable

* Returns: nothing

`displayTask(key, title, dueDate, taskStatus, dateGroup)` : Generates the HTML for each table row with the given parameters

* Returns: nothing

`loadTasks()` : Fetches the tasks from local storage and updates the sidebar badges 

* Returns: nothing

`getDateGroup(dueDate)` : Determines the date group of the given date, that is either 'today' or 'this week'. Otherwise it will return 'Unknown'

* Returns: dateGroup- string